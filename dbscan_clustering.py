import pandas as pd
import numpy as np

from sklearn.metrics.pairwise import cosine_similarity
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
import codecs
from collections import Counter, defaultdict
from pprint import pprint
import numpy as np
from sklearn.datasets.samples_generator import make_blobs
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import DBSCAN, KMeans
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import pairwise_distances
from matplotlib import pyplot as plt
import seaborn as sns
from collections import Counter
import time
from scipy.spatial.distance import cosine
sns.set()
import csv


def get_clusters_with_labeled_docs(y_test, y_pred):
    """
    Получаем на выходе структуру {cluster_N: list_of_docs,
    где cluster_N - номер кластера, list_of_docs - лейблы доков,
    попавших в кластер

    :param y_test:
    :param y_pred:
    :return:
    """
    clusters = defaultdict(list)

    for doc, cluster in enumerate(y_pred):
        clusters[cluster].append(y_test[doc])

    return clusters


def evaluate(y_test, y_pred):
    clusters = get_clusters_with_labeled_docs(y_test, y_pred)

    cluster_number_to_topic = {cluster: Counter(items).most_common(1)[0][0] for cluster, items in clusters.items()}
    y_pred_labels = [cluster_number_to_topic[y_i] for y_i in y_pred]

    pprint(clusters)
    print('-'*10)
    pprint(cluster_number_to_topic)
    print('-'*10)

    print(f1_score(y_test, y_pred_labels, average='micro'))


def evaluate_mean(y_test, y_pred):
    clusters = get_clusters_with_labeled_docs(y_test, y_pred)

    # Получаем количество документов по каждому топику
    topic_counts = Counter(y_test)

    counter_dict = defaultdict(list)

    f1_list = []
    for cluster, items in clusters.items():
        counter_dict[cluster] = Counter(items)
        cluster_topic = counter_dict[cluster].most_common(1)[0][0]

        true_positives = 0
        for item in items:
            if item == cluster_topic:
                true_positives += 1

        precision = true_positives / len(items)
        recall = true_positives / topic_counts[cluster_topic]
        f1 = f1_score(precision, recall)
        print(f'Cluster_topic: {cluster_topic}: {f1}')
        f1_list.append(f1)

    print(f'F1: {sum(f1_list) / len(f1_list)}')

'''
def kmeans(X_train, X_test, y_train, y_test):
    print("\nKMeans:")
    n_clusters = 80
    print("Number of clusters: %d" % n_clusters)
    kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(X_train)
    y_pred = kmeans.predict(X_test)
    evaluate(y_test, y_pred)
'''
def dbscan_params_selection(datasource):
    data = pd.read_csv(datasource, header=None)
    X = data.values[:, 0:-1]
    neigh = NearestNeighbors(n_neighbors=2)
    nbrs = neigh.fit(X)
    distances, indices = nbrs.kneighbors(X)
    distances = np.sort(distances, axis=0)
    distances = distances[:, 1]
    plt.plot(distances)
    plt.show()
    # max curve at 1.5 and 4

def cluster_dbscan(datasource):

    #DATA_WORDFORMS = '/home/kamivao/Downloads/lentanews/lentanews_split/hazards18354_docvec_wf_vec100_win10_count5.csv'
    data = pd.read_csv(datasource, header=None)

    X_train = data.values[:, 0:-1]
    labels = data.values[:, -1]
    start_time = time.time()
    clustering = DBSCAN(eps=0.7, min_samples=5).fit(X_train)
    end_time = time.time() - start_time
    print(end_time)
    predicted_clusters = clustering.labels_
    print(predicted_clusters)
    print(Counter(predicted_clusters))
    #X_train, X_test, y_train, y_test = train_test_split(features, labels, train_size=0.7, random_state=0)
    print("number of labels in data, number of predicted clusters: ", len(labels), len(predicted_clusters))

def cluster_kmeans(datasource):

    #DATA_WORDFORMS = '/home/kamivao/Downloads/lentanews/lentanews_split/hazards18354_docvec_wf_vec100_win10_count5.csv'
    data = pd.read_csv(datasource, header=None)

    X_train = data.values[:, 0:-1]
    labels = data.values[:, -1]
    start_time = time.time()
    clustering = KMeans(n_clusters=100).fit(X_train)
    end_time = time.time() - start_time
    print(end_time)
    y_kmeans = clustering.predict(X_train)
    plt.scatter(X_train[:, 0], X_train[:, 1], c=y_kmeans, s=50, cmap='viridis')

    centers = clustering.cluster_centers_
    plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)
    plt.show()

    cluster_7 = []
    for i in range(len(y_kmeans)):
        if y_kmeans[i] == 5:
            cluster_7.append(labels[i])

    #for x in sorted(cluster_7):
        #print(x)

    #predicted_clusters = clustering.labels_
    #print(predicted_clusters)
    #print(Counter(predicted_clusters))
    #X_train, X_test, y_train, y_test = train_test_split(features, labels, train_size=0.7, random_state=0)
    #print("number of labels in data, number of predicted clusters: ", len(labels), len(predicted_clusters))

def agglomerative_cosine(datasource, output_filepath):
    dataframe = pd.read_csv(datasource, header=None, nrows = 1000)
    #print(dataframe.shape)
    #print(dataframe.loc[[5]])
    X_train = dataframe.values[:, 0:-1]
    labels = dataframe.values[:, -1]
    start_time = time.time()
    model = AgglomerativeClustering(n_clusters=5, linkage="average", affinity="cosine")
    model.fit(X_train)
    y_agglomerative = model.fit_predict(X_train)
    cluster_7 = []

    output_writer = codecs.open(output_filepath, mode='w+', encoding='utf-8')
    for i in range(len(y_agglomerative)):
        if y_agglomerative[i] == 3:
            cluster_7.append(labels[i])
            #print(dataframe[i])
            #data_line = X_train[i]
            #data_line.append(labels[i])
            dataframe.loc[[i]].to_csv(output_writer, index=False, header=False)

    for x in sorted(cluster_7):
        print(x)
    output_writer.close()
    #output_writer = codecs.open(output_filepath, mode='w+', encoding='utf-8')

def similarity_calc(datasource):
    file = codecs.open(datasource, mode='r', encoding='utf-8')
    data = file.readlines()
    vectors = []
    labels = []

    for line in data:
        line = line.split(',')
        vector = np.asarray(line[0:-1], dtype='float64')
        label = line[-1]
        vectors.append(vector)
        labels.append(label)



    similarities_array = []
    f = codecs.open('/home/kamivao/Downloads/lentanews/lentanews_split/similarities_matric_c7.tsv', mode='w+', encoding='utf-8')
    tsv_writer = csv.writer(f, delimiter='\t')

    for i in range(len(vectors)):
        for j in range(i+1, len(vectors)):
            vi = vectors[i].reshape(1,-1)
            vj = vectors[j].reshape(1,-1)
            similarity = cosine_similarity(vi, vj)
            float_sim = similarity[0]
            if float_sim[0] > 0.8:
                print(float_sim)
            sim_info = [str(similarity), labels[i], labels[j]]

            tsv_writer.writerow(sim_info)
            similarities_array.append(similarity)

    f.close()
    #print(np.mean(similarities_array))
    sims = sorted(similarities_array, reverse=False)
    print(sims)








#dbscan_params_selection(datasource = '/home/kamivao/Downloads/lentanews/lentanews_split/hazards18354_docvec_wf_vec100_win10_count5.csv')
#cluster_dbscan(datasource='/home/kamivao/Downloads/lentanews/lentanews_split/hazards18354_docvec_wf_vec100_win10_count5.csv')
#cluster_kmeans(datasource='/home/kamivao/Downloads/lentanews/lentanews_split/hazards18354_docvec_wf_vec100_win10_count5.csv')
#agglomerative_cosine(datasource='/home/kamivao/Downloads/lentanews/lentanews_split/hazards18354_docvec_wf_vec100_win10_count5.csv',
 #                    output_filepath='/home/kamivao/Downloads/lentanews/lentanews_split/cluster_vectors.csv')

similarity_calc(datasource='/home/kamivao/Downloads/lentanews/lentanews_split/cluster_vectors.csv')











