from urllib.parse import urlparse
from collections import namedtuple
import re
import datetime
from collections import Counter


def parseObjectLabel(uri):
    objectLabel = urlparse(uri)
    dateSegment = objectLabel.path
    return dateSegment


def getDate(dateSegment):
    class_name = 'DateTuple'
    fields = 'year month day'
    DateTuple = namedtuple(class_name, fields)
    pathItems = re.split(r'/', dateSegment)
    date_i = datetime.date(int(pathItems[2]), int(pathItems[3]), int(pathItems[4]))
    return date_i

def subtract(excessive, total):
    efficient = []
    for ex in excessive:
        if ex in total:
            total.remove(ex)
    efficient = total
    return efficient


def main():
    try:
        datesList = []
        labelsList = ['https://lenta.ru/news/2018/12/12/nazran2/', 'https://lenta.ru/news/2018/11/12/nazran2/', 'https://lenta.ru/news/2018/12/16/nazran2/',
                      'https://lenta.ru/news/2018/12/17/nazran2/', 'https://lenta.ru/news/2018/11/25/nazran2/', 'https://lenta.ru/news/2018/12/01/nazran2/',
                      'https://lenta.ru/news/2018/09/12/nazran2/', 'https://lenta.ru/news/2018/09/15/nazran2/', 'https://lenta.ru/news/2018/11/26/nazran2/']
        #resulting timelines: [[0, 2, 3], [4, 5], [1]]
        for item in labelsList:
            dateSegment = parseObjectLabel(item)
            date_i = getDate(dateSegment)
            datesList.append(date_i)

        timelines = []
        for i in range(len(datesList)):
            timeline = []
            for j in range(i + 1, len(datesList)):
                delta = datesList[i] - datesList[j]
                diff = abs(delta.days)
                if diff < 11:
                    timeline.append(datesList[i])
                    timeline.append(datesList[j])
                    #datesList = datesList.remove(datesList[j])
            #datesList = datesList.remove(datesList[i])
            if len(timeline) != 0:
                timelines.append(set(timeline))


        print(timelines)
        print(len(timelines))

        excessive_timelines_indices = []
        for i in range(len(timelines)):
            if len(timelines[i]) != 0:
        # delete shorter timelines
                for j in range(i + 1, len(timelines)):
                    if timelines[i].issuperset(timelines[j]):
                        excessive_timelines_indices.append(j)
                    elif timelines[i].issubset(timelines[j]):
                        excessive_timelines_indices.append(timelines[i])

        print(excessive_timelines_indices)
        excessive_timelines_indices_set = set(excessive_timelines_indices)
        print(excessive_timelines_indices_set)

        excessive_timelines = []
        for index in excessive_timelines_indices_set:
            excessive_timelines.append(timelines[index])

        print(excessive_timelines)

        efficient = subtract(excessive=excessive_timelines, total=timelines)
        #https://www.tutorialspoint.com/How-to-sort-a-Python-date-string-list
        for tl in efficient:
                list(tl).sorted()
        print("Efficient timelines: ", efficient)


    except OSError as e:
        print(e)
        raise


if __name__ == "__main__":
    main()