import codecs, os
import csv
from urllib.parse import urlparse
import collections
import sys
import time
import string
from nltk import word_tokenize
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
import numpy as np

# decode, url_check - функции для проверки элементов csv-строки
def decode(str):
    try:
        str.decode("utf-8", "ignore")
        return True
    except Exception:
        return False


def url_check(url):
    try:
        result = urlparse(url)
        if all([result.scheme, result.netloc]):
            return True
        else:
            return False
    except Exception:
        return False


def csv_extract(datasource):
    csvfile = codecs.open(datasource, mode='r', encoding='utf-8', errors='ignore')
    # reader = csv.reader(csvfile, delimiter=';')
    uri_list, title_list, wordforms_doccollection, lemmas_doccollection = [], [], [], []
    diffs = []

    for line in csvfile:
        linelist = line.split(';')
        #print(linelist)
        count = 0

        uri = linelist[0]
        if url_check(uri) == True:
           count += 1

        title = linelist[1]
        if len(str(title)) > 5:
            count += 1

        text_wordforms = linelist[2]
        if len(text_wordforms) > 100:
            count += 1

        text_lemmas = linelist[3]
        if len(text_wordforms) > 100:
            count += 1

        if count == 4:
            uri_list.append(uri)
            title_list.append(title)
            wordforms_doccollection.append(text_wordforms)
            lemmas_doccollection.append(text_lemmas)

        diff = abs(len(text_wordforms) - len(text_lemmas))
        diffs.append(diff)
    print(collections.Counter(diffs))
    print(len(uri_list), len(text_wordforms), len(text_lemmas))
    csvfile.close()
    return uri_list, wordforms_doccollection, lemmas_doccollection


def doc2vec_modeling(uri_list, wordforms_doccollection, lemmas_doccollection, docvec_out_wordforms, docvec_out_lemmas):
    document_ids_list = uri_list

    translator = str.maketrans('', '', string.punctuation)


    wf_document_stream = [word_tokenize(doc) for doc in wordforms_doccollection]
    lem_document_stream = [word_tokenize(doc) for doc in lemmas_doccollection]

    wf_documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(wf_document_stream)]
    lem_documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(lemmas_doccollection)]


    wf_model = Doc2Vec(wf_documents, vector_size=100, window=5, min_count=1, workers=4, epochs=40)
    lem_model = Doc2Vec(lem_documents, vector_size=100, window=5, min_count=1, workers=4, epochs=40)


    #write document embeddings for lemmas with uri as label to outfile
    tsv_output_lemmas = csv.writer(docvec_out_lemmas, delimiter=',')
    ranks = []
    second_ranks = []
    array_of_sims = []
    for doc_id in range(len(lem_documents)):
        index = doc_id
        inferred_vector = lem_model.infer_vector(lem_documents[doc_id].words)
        inferred_vector = np.array(inferred_vector.tolist())
        data_line = np.append(inferred_vector, str(uri_list[index]))
        tsv_output_lemmas.writerow(data_line)
        sims = lem_model.docvecs.most_similar([inferred_vector], topn=len(lem_model.docvecs))
        rank = [docid for docid, sim in sims].index(doc_id)
        ranks.append(rank)
        array_of_sims.append(sims)

        second_ranks.append(sims[1])
        sims = []

    #write document embeddings for wordforms with uri as label to outfile
    tsv_output_wordforms = csv.writer(docvec_out_wordforms, delimiter=',')
    ranks = []
    second_ranks = []
    array_of_sims = []
    for doc_id in range(len(lem_documents)):
        index = doc_id
        inferred_vector = wf_model.infer_vector(wf_documents[doc_id].words)
        inferred_vector = np.array(inferred_vector.tolist())
        data_line = np.append(inferred_vector, str(uri_list[index]))
        tsv_output_wordforms.writerow(data_line)
        sims = wf_model.docvecs.most_similar([inferred_vector], topn=len(wf_model.docvecs))
        rank = [docid for docid, sim in sims].index(doc_id)
        ranks.append(rank)
        array_of_sims.append(sims)

        second_ranks.append(sims[1])
        sims = []

    print(collections.Counter(ranks))



def main():
    #if len(sys.argv) != 3:
     #   print("Wrong number of arguments: 2 arguments required")
    #else:

    try:
        outpath = '/home/kamivao/Downloads/lentanews'

        if not os.path.exists(outpath):
            os.makedirs(outpath)

        with open(os.path.join(outpath, 'stats.txt'), mode='a+', encoding='utf-8') as stats:
            start_time = time.time()
            uri_list, wordforms_docstream, lemmas_docstream = csv_extract(datasource='/home/kamivao/Downloads/lentanews_hazards.csv')
            stats.write("data loading finished in %s seconds " % (time.time() - start_time) + "\n")
            doc2vec_modeling(uri_list, wordforms_docstream, lemmas_docstream,
                             docvec_out_wordforms=open('/home/kamivao/Downloads/lentanews/docvec_dataset_wordform.csv', mode='w+', encoding='utf-8'),
                             docvec_out_lemmas=open('/home/kamivao/Downloads/lentanews/docvec_dataset_lemma.csv', mode='w+', encoding='utf-8'))
            stats.write("doc2vec modeling finished in %s seconds " % (time.time() - start_time) + "\n")
            start_time = time.time()
            stats.close()
        with open(os.path.join(outpath, 'dataset.tsv'), mode='w+', encoding='utf-8') as dataset_writer:
            for uri, text_wf, text_lem in zip(uri_list, wordforms_docstream, lemmas_docstream):
                #print(uri, text_wf, text_lem)
                dataset_writer.write(uri + '\t' + str(text_wf) + '\t' + str(text_lem) + '\n')
        dataset_writer.close()
    except OSError as e:
        print(e)
        raise


if __name__ == "__main__":
    main()
