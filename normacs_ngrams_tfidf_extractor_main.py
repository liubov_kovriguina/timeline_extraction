from gensim.models.phrases import Phraser
from gensim.models import Phrases
import time
import string, os
import numpy as np
from gensim.models import TfidfModel
from gensim.corpora import Dictionary
from nltk import word_tokenize
from collections import OrderedDict
from operator import itemgetter
import csv


def load_data(datasource, document_ids):
    # функция считывает все файлы, делает сегментацию и очистку, возвращаемые переменные:
    # collection - строковый массив, элемент массива - текст документа, очищенный от пунктуации,
    # document_stream - сегментированная коллекция документов, вложенный строковый массив, элемент внутреннего массива - токен,
    # document_ids_list - список имен файлов текстовой коллекции
    # datasource - путь к папке с документами (лемматизированными), из которых нужно извлечь n-граммы, формат - plain text
    # document_ids - tsv-файл с именами файлов из коллекции документов

    collection = []
    document_ids_list = []

    for entry in os.listdir(datasource):
        f = os.path.join(datasource, entry)
        with open(f, 'r', encoding='utf8') as file:
            contents = file.read().replace('\n', '')
            translator = str.maketrans('', '', string.punctuation)
            cleaned_file = contents.translate(translator)

            document_ids_list.append(entry)
            collection.append(cleaned_file)
            file.close()

    document_stream = [word_tokenize(doc) for doc in collection]
    document_stream = [list(filter(None, doc)) for doc in document_stream]

    with open(document_ids, mode='w+', encoding='utf-8') as doc_id_writer:
        for name in document_ids_list:
            doc_id_writer.write(str(name) + "\t")
    doc_id_writer.close()

    return collection, document_stream, document_ids_list


# build_tfidf_model() - функция строит tf-idf-модель текстовой коллекции, индексируя документы коллекции по ее общему словарю
# tfidf_model_out - текстовый файл для записи модели tf-idf
# model, corpus, collection_dictionary - модель, индексированый корпус текстовой коллекции, словарь текстовой коллекции
def build_tfidf_model(document_stream, tfidf_model_out):
    collection_dictionary = Dictionary(document_stream)

    corpus = [collection_dictionary.doc2bow(document) for document in document_stream]  # convert corpus to BoW format
    model = TfidfModel(corpus=corpus, dictionary=collection_dictionary, smartirs='ntc')  # fit model
    corpus_tfidf = model[corpus]

    with open(tfidf_model_out, mode='w+', encoding='utf-8') as model_writer:
        for item in corpus_tfidf:
            model_writer.write(str(item) + "\n")
    model_writer.close()
    return model, collection_dictionary, corpus


# ngram_extractor() - функция для извлечения n-грамм и их записи в файл
# bi, tri, tetra, penta, sexta - путь к файлам для записи словарей 2-, 3-, 4-, 5-, 6-грамм
# возвращает ngrams_dictionaries - список словарей n-грамм
def ngram_extractor(document_stream, bi, tri, tetra, penta, sexta):
    # ignoreterms- список слов, частота который не учитывается при расчете веса n-граммы
    ignoreterms = ['и', 'в', 'на', 'с', 'по', 'об', 'со', 'не', 'но', 'а', 'со', 'он', 'она', 'или']
    bigrams_dictionary = {}
    trigrams_dictionary = {}
    tetragram_dictionary, pentagram_dictionary, sextagram_dictionary = {}, {}, {}

    bigrams = Phrases(document_stream, scoring='default', min_count=3, delimiter=b' ', threshold=20,
                      common_terms=ignoreterms)  # train model
    biphraser = Phraser(bigrams)

    trigrams = Phrases(biphraser[document_stream], scoring='default', min_count=3, delimiter=b' ', threshold=20,
                       common_terms=ignoreterms)
    triphraser = Phraser(trigrams)


    # экспорт энграмм
    for bigram, score in bigrams.export_phrases(document_stream):
        bigrams_dictionary[bigram.decode('utf-8')] = score
    with open(bi, mode='w+', encoding='utf-8') as bigram_writer:
        sorted_bigrams_loglikelihood = (
            OrderedDict(sorted(bigrams_dictionary.items(), key=itemgetter(1), reverse=True)))
        # sort the keys according to the values:
        for key, val in sorted_bigrams_loglikelihood.items():
            bigram_writer.write('{}\t{}\n'.format(key, np.around(val, decimals=5)))
    bigram_writer.close()

    for component, score in getattr(triphraser, "phrasegrams", {}).items():
        ngram = " ".join(xgram.decode('utf8') for xgram in component)
        if ngram.count(' ') == 2:
            trigrams_dictionary[ngram] = score[1]

        elif ngram.count(' ') == 3:
            tetragram_dictionary[ngram] = score[1]

        elif ngram.count(' ') == 4:
            pentagram_dictionary[ngram] = score[1]

        elif ngram.count(' ') == 5:
            sextagram_dictionary[ngram] = score[1]

    with open(tri, mode='w+', encoding='utf-8') as trigram_writer:
        for key, val in trigrams_dictionary.items():
            trigram_writer.write('{}\t{}\n'.format(key, np.around(val, decimals=5)))

    with open(tetra, mode='w+', encoding='utf-8') as tetragram_writer:
        for key, val in tetragram_dictionary.items():
            tetragram_writer.write('{}\t{}\n'.format(key, np.around(val, decimals=5)))

    with open(penta, mode='w+', encoding='utf-8') as pentagram_writer:
        for key, val in pentagram_dictionary.items():
            pentagram_writer.write('{}\t{}\n'.format(key, np.around(val, decimals=5)))

    with open(sexta, mode='w+', encoding='utf-8') as sextagram_writer:
        for key, val in sextagram_dictionary.items():
            sextagram_writer.write('{}\t{}\n'.format(key, np.around(val, decimals=5)))

    trigram_writer.close()
    tetragram_writer.close()
    pentagram_writer.close()
    sextagram_writer.close()
    ngrams_dictionaries = [bigrams_dictionary, trigrams_dictionary, tetragram_dictionary, pentagram_dictionary,
                           sextagram_dictionary]
    del trigrams, triphraser, bigrams, biphraser
    return ngrams_dictionaries


# tfidf_annotator() - аннотирует энграммы значениями tf-idf
# result_out - выходной tsv-файл, в который записаны n-граммы, tfidf и значения log-likelihood, формат:
# [String \t Float \t String \t FloatList] --  n-gram \t n-gram score \t source_filename \t tfidf[1] \t ... tfidf[n], где n - количество слов в энграмме
# broken_out  - tsv-файл, в которые записываются энграммы, не валидированные по модели tf-idf, например, если у слова нулевой вес в модели

def tfidf_annotator(collection, corpus, collection_dictionary, document_ids_list, model, ngrams_dictionaries, result_out, broken_out):
    output_list = []
    #collection_dictionary_keys = collection_dictionary.keys()
    collection_dictionary_values = collection_dictionary.values()

    with open(result_out, 'w+', encoding='utf-8') as result, open(broken_out, 'w+', encoding='utf-8') as broken:
        for ngram_dictionary in ngrams_dictionaries:
            for key, val in sorted(ngram_dictionary.items()):
                for index in range(len(document_ids_list)):
                    if key in collection[index]:
                        output_list.append(key)
                        output_list.append(np.around(val, decimals=5))
                        output_list.append(document_ids_list[index])
                        list_of_unigrams = key.split(" ")
                        vector = model[corpus[index]]
                        converted_vector = dict(vector)

                        for unigram in list_of_unigrams:
                            unigram_position = list(collection_dictionary_values).index(unigram)
                            tf = converted_vector.get(unigram_position)
                            output_list.append(tf)

                        if None in output_list:
                            np.around(output_list[1], decimals=7)
                            tsv_broken_output = csv.writer(broken, delimiter='\t')
                            tsv_broken_output.writerow(output_list)

                        elif (None in output_list) == False:
                            np.around(output_list[1], decimals=7)
                            tsv_output = csv.writer(result, delimiter='\t')
                            tsv_output.writerow(output_list)

                        output_list = []

    broken.close()
    result.close()


def main():

    try:
        statspath = '/home/kamivao/Downloads/test_main'
        if not os.path.exists(statspath):
            statspath = os.makedirs(statspath)

        with open('/home/kamivao/Downloads/test_main/stats.txt', mode='a+', encoding='utf-8') as stats:
            start_time = time.time()
            collection, document_stream, document_ids_list = load_data(datasource='/home/kamivao/Downloads/test',
                                        document_ids='/home/kamivao/Downloads/test_main/doc_ids.tsv')
            stats.write("data loading finished in %s seconds " % (time.time() - start_time) + "\n")
            start_time = time.time()
            model, collection_dictionary, corpus = build_tfidf_model(document_stream, tfidf_model_out='/home/kamivao/Downloads/test_main/tfidf_model_file.txt')
            stats.write("tfidf modeling finished in %s seconds " % (time.time() - start_time) + "\n")
            ngrams_dictionaries = ngram_extractor(document_stream, '/home/kamivao/Downloads/test_main/bigrams_dictionary.txt',
                                                 '/home/kamivao/Downloads/test_main/trigrams_dictionary.txt', '/home/kamivao/Downloads/test_main/tetragrams_dictionary.txt',
                                                 '/home/kamivao/Downloads/test_main/pentagrams_dictionary.txt', '/home/kamivao/Downloads/test_main/sextagrams_dictionary .txt')
            start_time = time.time()
            stats.write("ngram extraction finished in %s seconds " % (time.time() - start_time) + "\n")
            tfidf_annotator(collection, corpus, collection_dictionary, document_ids_list, model, ngrams_dictionaries,
                            result_out='/home/kamivao/Downloads/test_main/result.csv', broken_out='/home/kamivao/Downloads/test_main/broken.csv')

            start_time = time.time()
            stats.write("tfidf annotation finished in %s seconds " % (time.time() - start_time) + "\n")

            stats.close()
    except OSError as e:
        print(e)
        raise


if __name__ == "__main__":
    main()





