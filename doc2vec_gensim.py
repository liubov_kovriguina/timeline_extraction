from gensim.test.utils import datapath
from gensim.models.word2vec import Text8Corpus
from gensim.models.phrases import Phraser
from gensim.models import Phrases
from gensim.test.utils import common_texts
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from collections import Counter
import time
import string, os
import gensim
import collections
import smart_open
import random
from gensim.models import TfidfModel
from gensim.corpora import Dictionary
from nltk import word_tokenize
from collections import OrderedDict
from operator import itemgetter
import csv
import numpy as np

#https://github.com/RaRe-Technologies/gensim/blob/develop/docs/notebooks/doc2vec-lee.ipynb

start_time = time.time()

# путь к папке с документами (лемматизированными), из которых нужно извлечь n-граммы
path_to_raw_text_collection = '/home/kamivao/Downloads/test'
# общий файл,  по которому рассчитываются n-граммы (объединение всех файлов в path_to_raw_text_collection)
output = '/home/kamivao/Downloads/again'


def doc2vec_modeling():
    text_collection = []
    document_ids_list = []
    stopwords = ['и', 'в', 'на', 'с', 'по', 'об', 'со', 'не', 'но', 'а', 'со', 'он', 'она', 'или']
    #stats = open('/home/kamivao/Downloads/stats.txt', mode='w', encoding='utf-8')
    translator = str.maketrans('', '', string.punctuation)
    docvec_out = open('/home/kamivao/Downloads/again/docvec_dataset.tsv', mode='w+', encoding='utf-8')

    for entry in os.listdir(path_to_raw_text_collection):
        f = os.path.join(path_to_raw_text_collection, entry)
        text = []
        with open(f, 'r', encoding='utf8') as file:
            content = " ".join(line.strip() for line in file)
            #content = file.readlines()
            #content = [x.strip() for x in content]
            translator = str.maketrans('', '', string.punctuation)
            content = content.translate(translator)
            #print(cleaned_content)
            document_ids_list.append(entry)

            text_collection.append(content)
            file.close()

    document_stream = [word_tokenize(doc) for doc in text_collection]
    for doc in document_stream:
        print(doc)
    documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(text_collection)]


    model = Doc2Vec(documents, vector_size=100, window=2, min_count=1, workers=4, epochs=40)
    #some_vector = model.infer_vector(['технический', 'требование', 'производство'])
    #sim = model.n_similarity(['жилой', 'здание'], ['жилой', 'помещение'])
    #print("{:.4f}".format(sim))
    #sim2 = model.docvecs.most_similar([some_vector])
    #print(sim2)

    tsv_output = csv.writer(docvec_out, delimiter='\t')
    ranks = []
    second_ranks = []
    array_of_sims = []
    for doc_id in range(len(documents)):
        index = doc_id
        inferred_vector = model.infer_vector(documents[doc_id].words)
        inferred_vector = np.array(inferred_vector.tolist())
        data_line = np.append(inferred_vector, str(index))
        tsv_output.writerow(data_line)
        sims = model.docvecs.most_similar([inferred_vector], topn=len(model.docvecs))
        rank = [docid for docid, sim in sims].index(doc_id)
        ranks.append(rank)
        array_of_sims.append(sims)

        second_ranks.append(sims[1])
        sims = []


    print(collections.Counter(ranks))

    #print('Document ({}): «{}»\n'.format(doc_id, ''.join(documents[doc_id].words)))
    #print(u'SIMILAR/DISSIMILAR DOCS PER MODEL %s:\n' % model)
    #for label, index in [('MOST', 0), ('MEDIAN', len(sims)//2), ('LEAST', len(sims) - 1)]:
    #    print(u'%s %s: «%s»\n' % (label, sims[index], ''.join(documents[sims[index][0]].words)))


doc2vec_modeling()



