from gensim.test.utils import datapath
from gensim.models.word2vec import Text8Corpus
from gensim.models.phrases import Phraser
from gensim.models import Phrases
from gensim.test.utils import common_texts
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
import time
import string, os
import gensim
import collections
import smart_open
import random
import numpy as np
from gensim.models import TfidfModel
from gensim.corpora import Dictionary
from nltk import word_tokenize
from collections import OrderedDict
from operator import itemgetter
import csv
import gc

def load_data(filepath):
    text_collection = []
    document_ids_list = []
    stopwords = ['и', 'в', 'на', 'с', 'по', 'об', 'со', 'не', 'но', 'а', 'со', 'он', 'она', 'или']
    # stats = open('/home/kamivao/Downloads/stats.txt', mode='w', encoding='utf-8')
    translator = str.maketrans('', '', string.punctuation)

    for entry in os.listdir(filepath):
        f = os.path.join(filepath, entry)
        text = []
        with open(f, 'r', encoding='utf8') as file:
            content = file.readlines()
            # content = [x.strip() for x in content]
            cleaned_content = [(x.strip()).translate(translator) for x in content]

            #cleaned_content = [word_tokenize(line) for line in cleaned_content]
            # print(cleaned_content)
            document_ids_list.append(entry)

            text_collection.append(cleaned_content)
            file.close()

    documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(text_collection)]
    for doc in documents:
        print(doc.words)

    return documents


def build_doc2vec_model(documents):
    model = Doc2Vec(documents, vector_size=50, window=2, min_count=1, workers=4, epochs=40)
    some_vector = model.infer_vector(['инженерный', 'изыскание', 'для ', 'строительство'])
    print(some_vector)

def main():
    documents = load_data('/home/kamivao/Downloads/test')
    build_doc2vec_model(documents)


if __name__ == "__main__":
    main()

