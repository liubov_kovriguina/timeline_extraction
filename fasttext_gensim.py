from gensim.models import FastText  # FIXME: why does Sphinx dislike this import?
from gensim.test.utils import common_texts  # some example sentences
from gensim.test.utils import get_tmpfile
from gensim.models.wrappers.fasttext import FastText as FT_wrapper
from gensim.models import KeyedVectors
from gensim.test.utils import datapath
import gensim.models
import codecs


#time_vector = model.wv['time']  # get vector for word
#interface_vector = model.wv['interface']
#[ 0.0114082   0.00190433 -0.07499751  0.02915386]
#[-0.00102198 -0.01546756 -0.03940643  0.05041444]

def train_getsimilar():
    training_file = codecs.open('/home/kamivao/Downloads/commands4000.txt', mode='r', encoding='utf-8')
    out_model_commands = codecs.open('/home/kamivao/Downloads/out_model_commands.txt', mode='w+', encoding='utf-8')
    out_model_similarities = codecs.open('/home/kamivao/Downloads/out_model_most_similar.txt', mode='w+', encoding='utf-8')


    data = training_file.readlines()
    text_collection = []
    for line in data:
        line = line.lower()
        line = line.strip('\n')
        linelist = line.split(' ')
        text_collection.append(linelist)

    model_commands = FastText(size=100, window=5, min_count=1)  # instantiate
    model_commands.build_vocab(sentences=text_collection)
    model_commands.train(sentences=text_collection, total_examples=len(text_collection), epochs=50)  # train


    for index in range(len(model_commands.wv.index2word)):
        out_model_commands.write(model_commands.wv.index2word[index]+'\t'+str(model_commands.wv.vectors[index])+'\n')
        #print(model_commands.wv.vectors[index])
        token = model_commands.wv.index2word[index]
        sims = model_commands.wv.most_similar(token)
        out_model_similarities.write(token+'\t'+str(sims)+'\n')
        #print(sims)


    temp_score = model_commands.wv.most_similar('температура')
    print(temp_score)
    hot_score = model_commands.wv.most_similar('жарко')
    print(hot_score)

    out_model_similarities.close()
    out_model_commands.close()
    model_commands.save("/home/kamivao/Downloads/ft.dim100win5minc1.model")


def play_pretrained():
    training_file = codecs.open('/home/kamivao/Downloads/commands4000.txt', mode='r', encoding='utf-8')
    out_model_commands = codecs.open('/home/kamivao/Downloads/out_model_commands.txt', mode='w+', encoding='utf-8')
    out_model_similarities = codecs.open('/home/kamivao/Downloads/out_model_most_similar.txt', mode='w+',
                                         encoding='utf-8')

    data = training_file.readlines()
    text_collection = []
    for line in data:
        line = line.lower()
        line = line.strip('\n')
        linelist = line.split(' ')
        text_collection.append(linelist)

    model_gensim = FastText(size=100, window=5, min_count=1)  # instantiate
    model_gensim.build_vocab(sentences=text_collection)
    model_gensim.train(sentences=text_collection, total_examples=len(text_collection), epochs=50)  # train

    print(model_gensim)

    # Set FastText home to the path to the FastText executable
    #ft_home = '/home/kamivao/Downloads/fastText-0.9.1/fasttext'

    #model_wrapper = FT_wrapper(sentences=text_collection, size=100, window=5, min_count=1)
    # train the model
    #model_wrapper.train(ft_home, text_collection)
    #print(model_wrapper)

    # saving a model trained via Gensim's fastText implementation
    model_gensim.save('/home/kamivao/Downloads/saved_model_gensim')
    loaded_model = FastText.load('/home/kamivao/Downloads/saved_model_gensim')
    print(loaded_model)

    print(loaded_model.wv.most_similar('температура'))
    print(loaded_model.wv.most_similar('жарко'))

    #loaded_model = FT_wrapper.load('/home/kamivao/Downloads/skipgram.model.vec')
    #model_bin = FastText.load_binary_data('/home/kamivao/Downloads/ft.dim100win5minc1.model')
    #fb_model = load_facebook_model(cap_path)
    #model = load_facebook_model(datapath('/home/kamivao/Downloads/ft.dim100win5minc1.model'))
    #model = gensim.models.fasttext.FastText.load_binary_data('/home/kamivao/Downloads/ft.dim100win5minc1.model')
    #model = KeyedVectors.load_facebook_model(datapath('/home/kamivao/Downloads/ft.dim100win5minc1.model'), binary=True,
          #                                   unicode_errors='ignore')




play_pretrained()






