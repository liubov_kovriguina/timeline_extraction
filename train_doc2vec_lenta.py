from news_clustering import url_check, decode
import codecs, os
import csv
from urllib.parse import urlparse
import collections
import sys
import time
import string
from nltk import word_tokenize
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
import numpy as np
import re, string



def preprocessing(datafile):
    translator = str.maketrans('', '', string.punctuation)
    uri_list, title_collection, wordforms_doccollection = [], [], []
    for line in datafile:
        line = line.replace('\"\"', '')
        linelist = line.split('\t')
        if len(linelist) == 3:

            title = linelist[1]
            text = linelist[2]
            if len(title) == 0:
                get_title = re.split(r'([^\s-]\"[А-Я])', text)
                if len(get_title) > 1:
                    #print(get_title[1])
                    s = text.find(get_title[1])
                    #print(s)
                    title_end_index = text.index(get_title[0])
                    text_start_index = text.index(get_title[1]) + 2
                    #print(text[0:s + 1])
                    #print(text[text_start_index:])
                    text_ex = text[text_start_index:]
                    title_ex = text[0:line.index(get_title[1]) + 1]
                    text_lcased = text_ex.lower()
                    title_lcased = title_ex.lower()
                    cleaned_text = text_lcased.translate(translator)
                    cleaned_title = title_lcased.translate(translator)
                    tokens_text = [x.strip() for x in cleaned_text.split()]
                    tokens_title = [x.strip() for x in cleaned_title.split()]
                    wordforms_doccollection.append(tokens_text)
                    title_collection.append(tokens_title)
                    uri_list.append(linelist[0])
            else:
                text_lcased = text.lower()
                title_lcased = title.lower()
                cleaned_text = text_lcased.translate(translator)
                cleaned_title = title_lcased.translate(translator)
                tokens_text = [x.strip() for x in cleaned_text.split()]
                tokens_title = [x.strip() for x in cleaned_title.split()]
                wordforms_doccollection.append(tokens_text)
                title_collection.append(tokens_title)
                uri_list.append(linelist[0])
    #print(len(wordforms_doccollection), len(title_collection))
    return uri_list, title_collection, wordforms_doccollection



def train_doc2vec(docvec_out_wordforms, docvec_out_titles):
    gen_uri_list, gen_title_collection, gen_wordforms_doccollection = [], [], []
    os.chdir('/home/kamivao/Downloads/lentanews/lentanews_split/reduced_csv_3/negative_sampling')
    for filename in os.listdir(os.getcwd()):
        datafile = codecs.open(filename, mode='r+', encoding='utf-8')
        uri_sample, title_sample, wordforms_sample = preprocessing(datafile)
        gen_uri_list += uri_sample
        gen_title_collection += title_sample
        gen_wordforms_doccollection += wordforms_sample

    print(len(gen_uri_list), len(gen_title_collection), len(gen_wordforms_doccollection))
    print(gen_uri_list[1019], gen_title_collection[1019], gen_wordforms_doccollection[1019])
    print(gen_uri_list[200019], gen_title_collection[200019], gen_wordforms_doccollection[200019])

    hazards_datasource = codecs.open('/home/kamivao/Downloads/lentanews/lentanews_split/hazards_dataset_3.tsv', mode='r+', encoding='utf-8')
    uri_hazards, title_hazards, wordforms_hazards = preprocessing(hazards_datasource)

    for i in range(len(uri_hazards)):
        uri = uri_hazards[i].strip("\r\n")

        if uri in gen_uri_list:
            id = gen_uri_list.index(uri)
            del gen_uri_list[id]
            del gen_title_collection[id]
            del gen_wordforms_doccollection[id]


    train_uri = gen_uri_list + uri_hazards
    train_titles = gen_title_collection + title_hazards
    train_wordforms = gen_wordforms_doccollection + wordforms_hazards

    wf_documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(train_wordforms)]
    wf_titles = [TaggedDocument(doc, [i]) for i, doc in enumerate(train_titles)]
    wf_model = Doc2Vec(wf_documents, vector_size=100, window=9, min_count=10, workers=10, epochs=100)
    print("Training finished!")

    # writing models to file
    docvec_out_wf = open(docvec_out_wordforms, mode='w+', encoding='utf-8')
    docvec_out_titles = open(docvec_out_titles, mode='w+', encoding='utf-8')

    tsv_output_wordforms = csv.writer(docvec_out_wf, delimiter=',')
    tsv_output_titles = csv.writer(docvec_out_titles, delimiter=',')

    # inferring vectors for titles and texts
    for doc in range(len(wf_documents)):
        index = doc
        inferred_vector = wf_model.infer_vector(wf_documents[doc].words)
        inferred_vector = np.array(inferred_vector.tolist())
        data_line = np.append(inferred_vector, str(train_uri[index]))
        tsv_output_wordforms.writerow(data_line)
    docvec_out_wf.close()

    for ttl in range(len(wf_titles)):
        index = ttl
        inferred_vector = wf_model.infer_vector(wf_titles[ttl].words)
        inferred_vector = np.array(inferred_vector.tolist())
        data_line = np.append(inferred_vector, str(train_uri[index]))
        tsv_output_titles.writerow(data_line)
    docvec_out_titles.close()





train_doc2vec(docvec_out_wordforms='/home/kamivao/Downloads/lentanews/lentanews_split/model3/vectors_for_texts_hazards_dim100win5minc4ep40.tsv',
              docvec_out_titles='/home/kamivao/Downloads/lentanews/lentanews_split/model3/vectors_for_titles_hazards_dim100win5minc4ep40.tsv')





'''
   
'''