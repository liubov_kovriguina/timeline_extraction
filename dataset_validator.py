from news_clustering import url_check, decode
import codecs, os
import csv
from urllib.parse import urlparse
import collections
import sys
import time
import string
from nltk import word_tokenize
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
import numpy as np
import re, string

#re.compile("(?<!^)\s+(?=[A-Z])(?!.\s)").split(s)

def filebreak(datasource):

    lines_per_file = 2000
    smallfile = None
    with codecs.open(datasource, mode='r', encoding='utf-8', errors='ignore') as bigfile:
        for lineno, line in enumerate(bigfile):
            if lineno % lines_per_file == 0:
                if smallfile:
                    smallfile.close()
                small_filename = 'small_file_{}.csv'.format(lineno + lines_per_file)
                smallfile = open(small_filename, "w")
            smallfile.write(line)
        if smallfile:
            smallfile.close()

def csv_extract(datasource, nonvalid):
    csvfile = codecs.open(datasource, mode='r', encoding='utf-8', errors='ignore')
    broken_writer = codecs.open(nonvalid, mode='w+', encoding='utf-8', errors='ignore')
    # reader = csv.reader(csvfile, delimiter=';')
    uri_list, title_list, wordforms_doccollection, lemmas_doccollection = [], [], [], []
    diffs = []

    for line in csvfile:
        #linelist = re.split(r'[^\s-];[^\s-]', line)
        linelist = re.split(r';', line)
        #print(len(linelist))
        #print(linelist)
        count = 0

        uri = linelist[0]
        if url_check(uri) == True:
           count += 1

        text_wordforms = linelist[2]
        text_lemmas = linelist[3]

        uri_list.append(uri)
        wordforms_doccollection.append(text_wordforms)
        lemmas_doccollection.append(text_lemmas)
        print(uri, text_wordforms[0:20] + ' .... ' + text_wordforms[-20:], text_lemmas[0:20] + ' .... ' + text_lemmas[-20:])
        #else:
         #   broken_writer.write(uri + '\t' + str(text_wordforms) + '\t' + str(text_lemmas) + '\n')

    print(len(uri_list), len(wordforms_doccollection), len(lemmas_doccollection))
    csvfile.close()
    return uri_list, wordforms_doccollection, lemmas_doccollection


def url_check(url):
    try:
        result = urlparse(url)
        if all([result.scheme, result.netloc]):
            return True
        else:
            return False
    except Exception:
        return False

def badmannered_csv_parsing(source_file, output_file):
    csvfile = codecs.open(source_file, mode='r', encoding='utf-8', errors='ignore')
    output_writer = codecs.open(output_file, mode='w+', encoding='utf-8', errors='ignore')
    # reader = csv.reader(csvfile, delimiter=';')
    uri, title, text = [], [], []

def custom_split(line):
    #line = 'https://lenta.ru/news/2018/11/30/su27/,Российские Су-27 перехватили над Балтикой,"ВВС Бельгии перехватили российские истребители Су-27 в небе над Балтийским морем.",Россия'
    line_items = re.split(r'[,A-Z]', line)
    #print(list)

def text2lemmas():
    os.chdir('/home/kamivao/Downloads/lentanews/lentanews_split/csv')
    print(os.getcwd())
    #texts_path = os.mkdir('/home/kamivao/Downloads/lentanews/lentanews_split/text')
    start_time = time.time()
    print(start_time)
    for filename in os.listdir(os.getcwd()):
        #file = codecs.open('/home/kamivao/Downloads/lentanews/lentanews_split/small_file_2000.csv', mode='r+', encoding='utf-8').readlines()
        file = codecs.open(filename, mode='r+', encoding='utf-8').readlines()
        name = filename.split('.')[0]

        text_file_name = name + '_lemmas' + '.txt'
        reduced_data_file_name = name + '_reduced' + '.tsv'

        with open(os.path.join('/home/kamivao/Downloads/lentanews/lentanews_split/text', text_file_name), mode='w+', encoding='utf-8') as tmp_text, \
                open(os.path.join('/home/kamivao/Downloads/lentanews/lentanews_split/reduced_csv_3', reduced_data_file_name), mode='w+', encoding='utf-8') as tmp_reduced:
            for line in file:
                line_items = re.split(r'[,A-Z]', line)
                line_v1 = line.replace(line_items[0]+',', '') # line_items[0] - урл
                curi = str(line_items[0])
                line_items = re.split(r'[,"]', line_v1)
                #print(line_items[0])
                title = str(line_items[0])
                line_v2 = line_v1.replace(line_items[0] + ',"', '')  # line_items[0] - урл
                #print(line_v2)
                line_items = re.split(r',', line_v2)
                line_items.remove(line_items[-1])
                if len(line_items) > 2:
                    line_items.remove(line_items[-1])

                for item in line_items:
                    new_text = ''.join(line_items)
                #print(new_text)
                if len(new_text) > 100  and url_check(url=curi) is True:
                    tmp_text.write(new_text + '\n')
                    tmp_reduced.write(curi +'\t' + title +'\t' + new_text + '\n')
        tmp_text.close()
        tmp_reduced.close()
    end_time = time.time()
    print(end_time)


def hazards_extraction():

    keywords = ['землетрясение', 'землетрясении', 'землетрясением', 'извержение', 'извержением', 'вулкан', 'вулкана', 'оползень', 'оползни', 'вода', 'снежной',
                'оползнем', 'сель', 'селевой', 'селевые', 'селями', 'лавина', 'лавины', 'лавиной', 'эрозия ', 'буря', 'бурей', 'ураган', 'урагана', 'землетрясения', 'ливнями', 'ливней',
                'ураганом', 'ураганный', 'ураганного', 'смерч', 'смерчем', 'шквал', 'шквальный', 'шторм', 'вихрь', 'вихрем', 'ливень', 'ливни', 'гроза', 'грозы',
                'грозой', 'грозами', 'снегопад', 'снегопадом', 'мороз', 'морозом', 'засуха', 'засухой', 'снежный', 'снежная', 'суховей', 'циклон', 'циклоном', 'циклонов', 'циклона', 'цунами',
                'наводнение', 'наводнением', 'наводнения', 'половодье', 'половодьем', 'затор', 'лед', 'льдом', 'воды', 'дождевой', 'поток', 'дождевые', 'потоки', 'потопление', 'грязевыми',
                'ветер', 'ветром', 'ветра', 'пожар', 'пожарами', 'пожары', 'пожаров', 'пожара', 'лесной', 'лесные', 'лесных', 'лесными', 'торфяной', 'торфяного', 'торфяных', 'горючие', 'ископаемые',
                'лава', 'лавой', 'лавы', 'лавовый', 'лавового', 'прорыв', 'плотина', 'плотины', 'дамба', 'дамбы', 'магнитуда', 'магнитудой', 'обрушение', 'здание', 'зданий', 'грязевой',
                'грязевого', 'поток', 'потока', 'потоком', 'потоки', 'обломки', 'горных', 'пород',
                'стихийного', 'бедствия', 'стихия', 'стихией', 'стихии', 'бедствии']
    keyword_set = set(keywords)
    output_filepath = '/home/kamivao/Downloads/lentanews/lentanews_split/hazards_dataset_3.tsv'
    output_writer = codecs.open(output_filepath, mode='w+', encoding='utf-8')
    os.chdir('/home/kamivao/Downloads/lentanews/lentanews_split/reduced_csv_3')
    start_time = time.time()

    translator = str.maketrans('', '', string.punctuation)
    keyword_occurence_distribution = []

    relevant_news_count = 0
    for filename in os.listdir(os.getcwd()):
        file = codecs.open(filename, mode='r+', encoding='utf-8')
        for line in file:
            #print(line)

            line_fields = line.split('\t')
            if len(line_fields) == 3:
                line_lcased = line_fields[2].lower()
                cleaned_line = line_lcased.translate(translator)
                tokens = [x.strip() for x in cleaned_line.split()]
                keyword_counter = list(keyword_set & set(tokens))
                keyword_occurence_distribution.append(len(keyword_counter))
                if len(keyword_counter) >= 2:
                    output_writer.write(line)
                keyword_counter = []
        file.close()

    print(collections.Counter(keyword_occurence_distribution))
    output_writer.close()
    runtime = (time.time() - start_time)
    print(runtime)
    #if len(keyword_counter) > 0:
             #   print(len(keyword_counter))

    '''
                        for keyword in hazards_keywords:
                #print(keyword)
                keyword.strip()
                if keyword in tokens:
                    print(keyword)
                    keyword_counter += 1
            if keyword_counter >=2:
                print(keyword_counter, line)
    '''








#csv_extract(datasource='/home/kamivao/Downloads/lentanews_hazards.csv', nonvalid='/home/kamivao/Downloads/lentanews/broken_input.tsv')
#filebreak(datasource='/home/kamivao/Downloads/lentanews/lenta-ru-news.csv')
#split()
#text2lemmas()
hazards_extraction()



