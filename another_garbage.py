import codecs
import re
import numpy as np


def regex():
    myfile = codecs.open(
        '/home/kamivao/Downloads/lentanews/lentanews_split/model2/model_titles_hazards_dim100win5minc4ep40.tsv',
        mode='r')
    #   head = [next(myfile) for x in range(20)]
    # print(head)

    title_vectors_dict = {}
    # titles_vectors = codecs.open(datasource_titles, mode='r', encoding='utf-8')
    titles_vectors_lines = myfile.readlines()
    tt_vectors = []
    for line in titles_vectors_lines:
        line = line.split(',')
        title_vectors_dict.update({str(line[-1]): line[0:-2]})

    for uri in title_vectors_dict:
        vec = title_vectors_dict.get(uri)
        vector = np.asarray(vec, dtype='float64')
        vector = vector.reshape(1, -1)
        if np.isinf(vector.any()):
            print("infinite", uri, vector)
        if np.isnan(vector.any()):
            print("NaN", uri, vector)
        if np.isneginf(vector.any()):
            print("neginf", uri, vector)

    myfile.close()


def enumer(text):
    tokens = text.split(' ')
    for idx, val in enumerate(tokens):
        print(idx, val)


def try_read():
    myfile = codecs.open(
        '/home/kamivao/Downloads/lentanews/lentanews_split/model3/model3texts.tsv', mode='r')
    head = [next(myfile) for x in range(20)]

    lines = myfile.readlines()
    print(len(lines))
    print(head)


def extract_vectors(all_text, all_titles, hazards):
    large_model_texts = codecs.open(all_text, mode='r', encoding='utf-8')
    large_model_titles = codecs.open(all_titles, mode='r', encoding='utf-8')
    only_hazards = codecs.open(hazards, mode='r', encoding='utf-8')
    out_hazards_texts = codecs.open('/home/kamivao/Downloads/lentanews/lentanews_split/model3/model3texts.tsv',
                                    mode='w+', encoding='utf-8')
    out_hazards_titles = codecs.open('/home/kamivao/Downloads/lentanews/lentanews_split/model3/model3titles.tsv',
                                     mode='w+',
                                     encoding='utf-8')
    data_texts = large_model_texts.readlines()
    data_titles = large_model_titles.readlines()

    hazards_vectors = only_hazards.readlines()
    count_tl = 0
    count_tt = 0
    uri_list = []

    for line in hazards_vectors:
        line = line.split(',')
        uri_list.append(str(line[-1]))

    for uri in uri_list:
        for line_texts in data_texts:
            if uri in line_texts:
                print(uri, line_texts)
                out_hazards_texts.write(line_texts)
                count_tt += 1
    out_hazards_texts.close()

    for uri in uri_list:
        for line_titles in data_titles:
            if uri in line_titles:
                out_hazards_titles.write(line_titles)
                count_tl += 1

    print(count_tt, count_tl)

    out_hazards_titles.close()


try_read()

# enumer(text="включай стиральную машину в 23:05 каждое четное число месяца")

