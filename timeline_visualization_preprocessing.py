import csv
import codecs
from urllib.parse import urlparse
import re
import datetime
from collections import Counter, defaultdict, namedtuple
from sklearn.metrics.pairwise import cosine_similarity
from scipy.stats.mstats import gmean
from scipy.stats import hmean
import numpy as np
import itertools


def parseObjectLabel(uri):
    objectLabel = urlparse(uri)
    dateSegment = objectLabel.path
    return dateSegment


def getDate(dateSegment):
    class_name = 'DateTuple'
    fields = 'year month day'
    DateTuple = namedtuple(class_name, fields)
    pathItems = re.split(r'/', dateSegment)
    date_i = datetime.date(int(pathItems[2]), int(pathItems[3]), int(pathItems[4]))
    return date_i


def viz_preproc(hazards_dataset, timelines_uri, timelines_viewer, only_merged_timelines_viewer, final_timelines_viewer):

    datasource = codecs.open(hazards_dataset, mode='r', encoding='utf-8', errors='ignore')
    timelines = codecs.open(timelines_uri, mode='r', encoding='utf-8', errors='ignore')
    #tl_viewer = codecs.open(timelines_viewer, mode='w+', encoding='utf-8', errors='ignore', newline='')
    #tsv_timeline_step_writer = csv.writer(tl_viewer, delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='', escapechar='\\')

    tl_viewer =  open(timelines_viewer, mode='w+', encoding='utf-8',  newline='')
    tsv_timeline_step_writer = csv.writer(tl_viewer, delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='', escapechar='\\', lineterminator='')



    data = datasource.readlines()
    timelines_set = timelines.readlines()

    data_preprocessed = []
    for line in data:
        line = line.split('\t')
        data_preprocessed.append(line)

    # a dictionary with source dataset with uri as a key and title and text as value
    dataset_dictionary = {item[0]: item[1:] for item in data_preprocessed}

    visualization_friendly_timeline_representation = []

    counter = 1
    for sequence in timelines_set:
        #tsv_timeline_step_writer.writerow("Timeline %d" %counter)
        tl_viewer.write("Timeline %d \n" % counter)
        sequence = sequence.replace('\'', '').lstrip()
        stripped = sequence[1:-2]

        uri_list = stripped.split(',')
        preprocessed_uri_list = [uri.lstrip() for uri in uri_list]
        sorted_timeline = sorted(preprocessed_uri_list)

        full_timeline_representation = []
        for uri in sorted_timeline:
            timeline_step_items = []
            timeline_step_items.append(uri)
            if uri in dataset_dictionary.keys():

                tmp = dataset_dictionary.get(uri)
                timeline_step_items.append(tmp[0])
                timeline_step_items.append(tmp[1])
                date_segment = parseObjectLabel(uri=uri)
                date_i = getDate(dateSegment=date_segment)
                timeline_step_items.insert(0, date_i)
            full_timeline_representation.append(timeline_step_items)
            # write timeline items to outfile
            tsv_timeline_step_writer.writerow(timeline_step_items)


        visualization_friendly_timeline_representation.append(full_timeline_representation)
        # write delimiters to outfile
        tsv_timeline_step_writer.writerow('\n\n')
        counter += 1


        # matching text data from documents collection and date extraction
        # result format: date \t title \t text \t uri

    print(len(visualization_friendly_timeline_representation))
    #return visualization_friendly_timeline_representation

    # short (len<4) timelines merging
    #############   MERGING   ###############
    vectors_dictionary = {}
    #'/home/kamivao/Downloads/lentanews/lentanews_split/model2/model_hazards_dim100win5minc4ep40.tsv',
    document_vectors = codecs.open('/home/kamivao/Downloads/lentanews/lentanews_split/model4/model4_text_vectors.tsv', mode='r', encoding='utf-8', errors='ignore')
    #merged_timelines_outfile = codecs.open('/home/kamivao/Downloads/lentanews/lentanews_split/model2/merged_timelines.tsv', mode='w+', encoding='utf-8', errors='ignore')
    # tl_viewer = codecs.open(timelines_viewer, mode='w+', encoding='utf-8', errors='ignore', newline='')
    # tsv_timeline_step_writer = csv.writer(tl_viewer, delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='', escapechar='\\')

    vectors_lines = document_vectors.readlines()
    vectors = []
    for line in vectors_lines:
        line = line.split(',')
        vectors_dictionary.update({str(line[-1]).strip('\r\n'): line[0:-2]})

    print("Vectors loaded")

    # список объединенных таймлайнов

    merge_candidates_data = []
    candidates_for_merging_sims_values = []
    for i in range(len(visualization_friendly_timeline_representation)):
        for j in range(i+1, len(visualization_friendly_timeline_representation)):
            uri_list_timeline_a, uri_list_timeline_b = [], []
            dates_list_timeline_a, dates_list_timeline_b = [], []

            timeline_a = visualization_friendly_timeline_representation[i]
            timeline_b = visualization_friendly_timeline_representation[j]

            if len(timeline_a) < 10 and len(timeline_b) < 10:
                for timeline_step in timeline_a:
                    uri_list_timeline_a.append(timeline_step[1])
                    #dates_list_timeline_a.append(timeline_step[0])
                for timeline_step_ in timeline_b:
                    uri_list_timeline_b.append(timeline_step_[1])
                    #dates_list_timeline_b.append(timeline_step_[0])

                # проверяется наличие общих документов в таймлайнах, должен быть, как минимум, 1
                common_items = set.intersection(set(uri_list_timeline_a), set(uri_list_timeline_b))
                if len(common_items) > 0:
                    # объединение множеств uri двух таймлайнов

                    timelines_union = set(uri_list_timeline_a).union(set(uri_list_timeline_b))
                    timelines_union_list = list(timelines_union)

                    pairwise_similarity_values = []
                    for x in range(len(timelines_union_list)):
                        vector_x = np.asarray(vectors_dictionary.get(timelines_union_list[x]), dtype='float64')
                        for y in range(x + 1, len(timelines_union_list)):
                            vector_y = np.asarray(vectors_dictionary.get(timelines_union_list[y]), dtype='float64')
                            vx = vector_x.reshape(1, -1)
                            vy = vector_y.reshape(1, -1)
                            similarity = cosine_similarity(vx, vy)
                            pairwise_similarity_values.append(similarity)
                    #timeline_average_similarity_value = sum(pairwise_similarity_values) / float(len(pairwise_similarity_values))
                    timeline_hmean_similarity_value = hmean(pairwise_similarity_values)
                    candidates_for_merging_sims_values.append(timeline_hmean_similarity_value)
                    # в мерж-кортеж записываются только пары таймлайнов:
                    # ([объединенный таймлайн], [индексы таймлайнов в исходном множестве таймлайнов], значение среднего косинусного сходства векторов таймлайнов)
                    paired_timelines_data = timeline_a
                    for step in timeline_b:
                        if step not in paired_timelines_data:
                            paired_timelines_data.append(step)
                    #paired_timelines_data.extend(x for x in timeline_b if x not in paired_timelines_data)
                    indices = [i, j]
                    pair_data = (paired_timelines_data, indices, timeline_hmean_similarity_value)
                    merge_candidates_data.append(pair_data)

    sorted_candidates_sims = sorted(list(candidates_for_merging_sims_values))
    #print(sorted_candidates_sims)
    #print('ariphmetic mean: ', sum(sorted_candidates_sims) / float(len(sorted_candidates_sims)))
    #print('geometric mean: ', gmean(sorted_candidates_sims))
    #print('harmonic mean: ', hmean(sorted_candidates_sims))
    #print('median: ', np.median(sorted_candidates_sims))
    harmonic_mean = hmean(sorted_candidates_sims)
    hm_theta = harmonic_mean[0].astype(float)
    hm_theta = np.round(hm_theta, decimals=5)
    print(hm_theta)

    # фильтрация смерженных таймлайнов-кандидатов по пороговому значению,
    # заполнение списка индексов слитых таймлайнов (для удаления из visualization_friendly_timeline_representation)
    merged_timelines_indices = []
    visualization_friendly_merged_timelines = []
    for merge in merge_candidates_data:
        # если сходство между док-ми смерженного таймлайна больше порогового -> постобработка к выходному формату
        if merge[2] > (hm_theta + 0.005*hm_theta) and merge[1] not in visualization_friendly_merged_timelines:
            merged_timelines_indices.append(merge[1])

            sorted_merged_timeline_representation = sorted(merge[0])
            visualization_friendly_merged_timelines.append(sorted_merged_timeline_representation)

    # removing duplicates from a list of merged timelines
    print(len(visualization_friendly_merged_timelines))
    no_duplicates_merged_timelines = []
    for mtl in visualization_friendly_merged_timelines:
        if not (mtl in no_duplicates_merged_timelines):
            no_duplicates_merged_timelines.append(mtl)
    print(len(no_duplicates_merged_timelines))

            #print(merge[1])
    merged_timelines_indices_concatenated = []
    for item in merged_timelines_indices:
        if not (item[0] in merged_timelines_indices_concatenated):
            merged_timelines_indices_concatenated.append(item[0])
        if not (item[1] in merged_timelines_indices_concatenated):
            merged_timelines_indices_concatenated.append(item[1])

    sorted_indices_rm = sorted(merged_timelines_indices_concatenated)

    timelines_to_be_removed = []
    for index in sorted_indices_rm:
        timelines_to_be_removed.append(visualization_friendly_timeline_representation[index])
    for item in timelines_to_be_removed:
        visualization_friendly_timeline_representation.remove(item)


    print("writing of final version and merged subset has started...")

    only_merged_tl_viewer = open(only_merged_timelines_viewer, mode='w+', encoding='utf-8', newline='')
    only_merged_tsv_timeline_step_writer = csv.writer(only_merged_tl_viewer, delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='',
                                          escapechar='\\', lineterminator='')

    final_tl_viewer = open(final_timelines_viewer, mode='w+', encoding='utf-8', newline='')
    final_tsv_timeline_step_writer = csv.writer(final_tl_viewer, delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='',
                                          escapechar='\\', lineterminator='')

    counter = 1
    for tl in no_duplicates_merged_timelines:
        only_merged_tl_viewer.write("Timenline %d \n" % counter)
        for abcde in tl:
            only_merged_tsv_timeline_step_writer.writerow(abcde)
        only_merged_tl_viewer.write('\n\n')
        counter += 1
    only_merged_tl_viewer.close()

    counter = 1
    for items in itertools.chain(no_duplicates_merged_timelines, visualization_friendly_timeline_representation):
        final_tl_viewer.write("Timenline %d \n" % counter)
        for step in items:
            final_tsv_timeline_step_writer.writerow(step)
        final_tl_viewer.write('\n\n')
        counter += 1

    tl_viewer.close()
    #only_merged_tl_viewer.close()
    final_tl_viewer.close()
'''
    print("Check for wrong merges: ")
    vectors_timeline3 = []
    for tl in no_duplicates_merged_timelines[7]:
        print(tl)
        vector = np.asarray(vectors_dictionary.get(tl[1]), dtype='float64')
        vectors_timeline3.append(vector)

        for x in range(len(vectors_timeline3)):
            for y in range(x + 1, len(vectors_timeline3)):
                vx = vectors_timeline3[x].reshape(1, -1)
                vy = vectors_timeline3[y].reshape(1, -1)
                similarity = cosine_similarity(vx, vy)
                print(x, y, similarity)
'''





viz_preproc(hazards_dataset='/home/kamivao/Downloads/lentanews/lentanews_split/model4/hazards_dataset.tsv',
         timelines_uri='/home/kamivao/Downloads/lentanews/lentanews_split/model4/run5/efficient_timelines_uri.tsv',
         timelines_viewer='/home/kamivao/Downloads/lentanews/lentanews_split/model4/run5/all_timelines_view_data.tsv',
         only_merged_timelines_viewer='/home/kamivao/Downloads/lentanews/lentanews_split/model4/run5/only_merged_timelines_view_data.tsv',
         final_timelines_viewer='/home/kamivao/Downloads/lentanews/lentanews_split/model4/run5/aftermerge_timelines_view_data_run4.tsv')





'''
def main():

----
    main(datasource='/home/kamivao/Downloads/lentanews/lentanews_split/model2/model_hazards_dim100win5minc4ep40.tsv',
         pairwise_sim_outfile='/home/kamivao/Downloads/lentanews/lentanews_split/model2/similarities_matrix.tsv',
         timelines_outfile='/home/kamivao/Downloads/lentanews/lentanews_split/model2/efficient_timelines_uri.tsv',
         all_timelines_outfile='/home/kamivao/Downloads/lentanews/lentanews_split/model2/all_timelines_uri.tsv',
         excessive_timelines_indices_outfile='/home/kamivao/Downloads/lentanews/lentanews_split/model2/excessive_timelines_indices.tsv')
----

if __name__ == "__main__":
    main(datasource='/home/kamivao/Downloads/lentanews/lentanews_split/hazards18354_docvec_wf_vec100_win10_count5.csv',
         hazards_dataset='/home/kamivao/Downloads/lentanews/lentanews_split/hazards_dataset_3.tsv',
         timelines_uri='/home/kamivao/Downloads/lentanews/lentanews_split/efficient_timelines_uri.tsv')
         
            
'''


