import codecs
import string
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
import numpy as np
import csv
import re


#index_to_doctag(i_index)
#Get string key for given i_index, if available. Otherwise return raw int doctag (same int).

def doc2vec_modeling(datasource, docvec_out_wordforms, docvec_out_titles):
    datafile = codecs.open(datasource, mode='r')
    docvec_out_wf = open(docvec_out_wordforms, mode='w+', encoding='utf-8')
    docvec_out_titles = open(docvec_out_titles, mode='w+', encoding='utf-8')
    translator = str.maketrans('', '', string.punctuation)
    uri_list, title_collection, wordforms_doccollection = [], [], []

    for line in datafile:
        line = line.replace('\"\"', '')
        linelist = line.split('\t')
        uri_list.append(linelist[0])
        title = linelist[1]
        text = linelist[2]
        if len(title) == 0:
            get_title = re.split(r'([^\s-]\"[А-Я])', text)
            if len(get_title) > 1:
                print(get_title[1])
                s = text.find(get_title[1])
                print(s)
                title_end_index = text.index(get_title[0])
                text_start_index = text.index(get_title[1]) + 2
                print(text[0:s+1])
                print(text[text_start_index:])
                text_ex = text[text_start_index:]
                title_ex = text[0:line.index(get_title[1]) + 1]
                text_lcased = text_ex.lower()
                title_lcased = title_ex.lower()
                cleaned_text = text_lcased.translate(translator)
                cleaned_title = title_lcased.translate(translator)
                tokens_text = [x.strip() for x in cleaned_text.split()]
                tokens_title = [x.strip() for x in cleaned_title.split()]
                wordforms_doccollection.append(tokens_text)
                title_collection.append(tokens_title)
        else:
            text_lcased = linelist[2].lower()
            title_lcased = linelist[1].lower()
            cleaned_text = text_lcased.translate(translator)
            cleaned_title = title_lcased.translate(translator)
            tokens_text = [x.strip() for x in cleaned_text.split()]
            tokens_title = [x.strip() for x in cleaned_title.split()]
            wordforms_doccollection.append(tokens_text)
            title_collection.append(tokens_title)
    print(len(wordforms_doccollection), len(title_collection))
    print(wordforms_doccollection[0:2], title_collection[0:2])

    wf_documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(wordforms_doccollection)]
    wf_titles = [TaggedDocument(doc, [i]) for i, doc in enumerate(title_collection)]
    wf_model = Doc2Vec(wf_documents, vector_size=100, window=5, min_count=4, workers=4, epochs=40)
    print("Training finished!")

    tsv_output_wordforms = csv.writer(docvec_out_wf, delimiter=',')
    tsv_output_titles = csv.writer(docvec_out_titles, delimiter=',')
    # inferring vectors for titles and texts
    for doc in range(len(wf_documents)):
        index = doc
        inferred_vector = wf_model.infer_vector(wf_documents[doc].words)
        inferred_vector = np.array(inferred_vector.tolist())
        data_line = np.append(inferred_vector, str(uri_list[index]))
        tsv_output_wordforms.writerow(data_line)
    docvec_out_wf.close()

    for ttl in range(len(wf_titles)):
        index = ttl
        inferred_vector = wf_model.infer_vector(wf_titles[ttl].words)
        inferred_vector = np.array(inferred_vector.tolist())
        data_line = np.append(inferred_vector, str(uri_list[index]))
        tsv_output_titles.writerow(data_line)
    docvec_out_titles.close()


'''

    wf_document_stream = [word_tokenize(doc) for doc in wordforms_doccollection]
    lem_document_stream = [word_tokenize(doc) for doc in lemmas_doccollection]

    wf_documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(wf_document_stream)]
    lem_documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(lemmas_doccollection)]


    wf_model = Doc2Vec(wf_documents, vector_size=100, window=5, min_count=1, workers=4, epochs=40)
    lem_model = Doc2Vec(lem_documents, vector_size=100, window=5, min_count=1, workers=4, epochs=40)
 tsv_output_wordforms = csv.writer(docvec_out_wordforms, delimiter=',')
    ranks = []
    second_ranks = []
    array_of_sims = []
    for doc_id in range(len(lem_documents)):
        index = doc_id
        inferred_vector = wf_model.infer_vector(wf_documents[doc_id].words)
        inferred_vector = np.array(inferred_vector.tolist())
        data_line = np.append(inferred_vector, str(uri_list[index]))
        tsv_output_wordforms.writerow(data_line)
        sims = wf_model.docvecs.most_similar([inferred_vector], topn=len(wf_model.docvecs))
        rank = [docid for docid, sim in sims].index(doc_id)
        ranks.append(rank)
        array_of_sims.append(sims)

        second_ranks.append(sims[1])
        sims = []

    print(collections.Counter(ranks))
'''



doc2vec_modeling(datasource='/home/kamivao/Downloads/lentanews/lentanews_split/hazards_dataset_3.tsv',
                 docvec_out_wordforms='/home/kamivao/Downloads/lentanews/lentanews_split/vectors_for_texts_hazards_dim100win5minc4ep40.tsv',
                 docvec_out_titles='/home/kamivao/Downloads/lentanews/lentanews_split/vectors_for_titles_hazards_dim100win5minc4ep40.tsv')