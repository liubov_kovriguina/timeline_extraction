import os, codecs
import timeline_visualization_preprocessing


def select_timelines(input):
    tl = codecs.open(input,
                     mode='r',encoding='utf-8')
    timelines = tl.readlines()

    ds = codecs.open('/home/kamivao/Downloads/lentanews/lentanews_split/model4/hazards_dataset.tsv',
                     mode='r', encoding='utf-8')
    dataset = ds.readlines()

    timelinesList = []
    indices = []
    index = []

    for i in range(len(timelines)-1):
        if (timelines[i] == '\n' and timelines[i+1] == '\n'):
            index.append(i)
            index.append(i+1)
            indices.append(index)
            index = []

    initial_timeline_end = indices[0][0]
    first_timeline = timelines[1:initial_timeline_end]
    timelinesList.append(first_timeline)

    for i in range(1,len(indices)):
        begin = indices[i-1][1] + 2
        end = indices[i][0]
        timeline = timelines[begin:end]
        timelinesList.append(timeline)
    print("piu")
    #print(len(timelines)

    return timelinesList


def merge_overlapping_timelines(timelinesToMerge, outfile):
    ignored_indices, tmp = [], []
    mergedIndices = []
    aftermergeTimelines = []
    merge_result = set()

    for i in range(len(timelinesToMerge)):
        itemsToMerge = []
        head = set(timelinesToMerge[i]) # get head

        if i not in ignored_indices:

            ignored_indices.append(i)
            for j in range(i+1, len(timelinesToMerge)): # try attaching timelines to head
                if j not in ignored_indices:
                    tail = set(timelinesToMerge[j])      # get tail

                    if len(head.intersection(tail)) > 1:        # compare
                        merge = head.union(tail)
                        mergedIndices.append(i)
                        mergedIndices.append(j)
                        print("merged i, j : ", i, j)

                        merge_result = merge_result.union(merge)
                        #head = merged       # update head
                        ignored_indices.append(j)


            aftermergeTimelines.append(list(sorted(merge_result))) #
            merge_result = set()
            merged = ()

    mergedTimelines = set(mergedIndices)
    final_tl_viewer = open(outfile, mode='w+', encoding='utf-8', newline='')
    #final_tsv_timeline_step_writer = csv.writer(final_tl_viewer, delimiter='\t', quoting=csv.QUOTE_NONE, quotechar='',
                                         #       escapechar='\\', lineterminator='')

    counter = 1
    for tl in aftermergeTimelines:
        if len(tl) > 0:
            final_tl_viewer.write("Timenline %d \n" % counter)
            tl = sorted(tl)
            for step in tl:
                final_tl_viewer.write(step)
            final_tl_viewer.write('\n\n')
            counter += 1
    for id in range(len(timelinesToMerge)):
        if id not in mergedTimelines:
            final_tl_viewer.write("Timenline %d \n" % counter)
            tl = sorted(timelinesToMerge[id])
            for step in tl:
                final_tl_viewer.write(step)
            final_tl_viewer.write('\n\n')
            counter += 1

    final_tl_viewer.close()

    print("piu")




def main():
    timelinesList = select_timelines(input='/home/kamivao/Downloads/lentanews/lentanews_split/model4/run5/aftermerge_timelines_view_data_run5.tsv')
    merge_overlapping_timelines(timelinesToMerge=timelinesList,outfile='/home/kamivao/Downloads/lentanews/lentanews_split/model4/run5/advanced_merge_result.tsv')

if __name__ == "__main__":
    main()
#select_timelines()



'''
def merge_overlapping_timelines(timelinesToMerge, outfile):
    ignored_indices, tmp = [], []
    aftermergeTimelines = []

    for i in range(len(timelinesToMerge)):
        itemsToMerge = []
        head = set(timelinesToMerge[i]) # get head

        if i not in ignored_indices:
            print("Len head : ", len(head))
            ignored_indices.append(i)
            for j in range(i+1, len(timelinesToMerge)): # try attaching timelines to head
                if j not in ignored_indices:
                    tail = set(timelinesToMerge[j])      # get tail
                    print("Len tail : ", len(tail))
                    if len(head.intersection(tail)) > 3:        # compare
                        merged = head.union(tail)
                        print("merged i, j : ", i, j)
                        print(len(merged))
                        head = merged       # update head
                        ignored_indices.append(j)


            aftermergeTimelines.append(list(merged)) #
            merged = ()

    final_tl_viewer = open(outfile, mode='w+', encoding='utf-8', newline='')

'''