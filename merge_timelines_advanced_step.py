import os, codecs,re
import timeline_visualization_preprocessing
import timelines_manual_eval
from collections import Counter, namedtuple, OrderedDict
import datetime
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np


def extract_date(sequence):
    metaItems = sequence.split('\t')
    dateSegment = timeline_visualization_preprocessing.parseObjectLabel(uri=metaItems[0])
    date_i = timeline_visualization_preprocessing.getDate(dateSegment)
    return metaItems, date_i

def sort_dataset(dataset):
    data = codecs.open(dataset, mode='r', encoding='utf-8')
    news = data.readlines()
    augmentedDataset = []

    for line in news:
        metaItems, date_i = extract_date(sequence=line)
        metaItems.insert(0, str(date_i))
        augmentedLine = '\t'.join(metaItems)
        augmentedDataset.append(augmentedLine)


    sortedDataset = sorted(augmentedDataset)
    print("piu-piu, dataset sorted!")
    return sortedDataset


def resort_long_timelines(input_file):
    #f = codecs.open(input_file, mode='r', encoding='utf-8')
    #data = f.readlines()
    longTimelines = timelines_manual_eval.select_timelines(input_file)
    lengthOfTimelinesWithDuplicates = []
    lengthOfTimelinesWithoutDuplicates = []

    for timeline in longTimelines:
        if len(timeline) < 3:
            longTimelines.remove(timeline)
        else:
            lengthOfTimelinesWithDuplicates.append(len(timeline))

    print(Counter(lengthOfTimelinesWithDuplicates))
    print(len(longTimelines))

    sortedLongTimelines = []
    for timeline in longTimelines:
        no_duplicates = set(timeline)
        timeline = list(sorted(no_duplicates))  #
        lengthOfTimelinesWithoutDuplicates.append(len(timeline))
        sortedLongTimelines.append(timeline)

    print("long ready!")
    print(Counter(lengthOfTimelinesWithoutDuplicates))
    print(len(sortedLongTimelines))
    return sortedLongTimelines


def extract_context(hazards_dataset, automerged_timelines):

    datasource = sort_dataset(dataset=hazards_dataset)
    context_list, news_item = [], []
    #for line in datasource:
     #   news_item.append(line[0])
      #  news_item.append(line)
       # context_list.append(news_item)
    #sorted_context_dictionary = OrderedDict(sorted(context_dictionary.items()))

    autoTimelines = resort_long_timelines(input_file=automerged_timelines)
    context, event_duration_count = [], []
    for aTimeline in autoTimelines:
        try:
            length = len(aTimeline)
            items_first = aTimeline[0].split('\t')
            strBeginDate = items_first[0]
            items_last = aTimeline[length - 1].split('\t')
            strEndDate = items_last[0]

            dtBeginDate = datetime.strptime(strBeginDate, '%Y-%m-%d')
            dtEndDate = datetime.strptime(strEndDate, '%Y-%m-%d')
            delta = dtBeginDate - dtEndDate
            diff = abs(delta.days)
            event_duration_count.append(diff)

            #if diff < 10:
             #   contextStartIndex = datasource.index(strBeginDate)
              #  contextEndIndex = len(datasource) - datasource[::-1].index(strEndDate) - 1
              #  print("context: ", contextStartIndex, contextEndIndex)

            #if diff > 150:
             #   print("timeline index : ", autoTimelines.index(aTimeline))
        except:
            print("Broken format?: ", strBeginDate, dtBeginDate, strEndDate, dtEndDate)






def visualize_timeline_duration(event_duration_count):
    print("diff  calculated!")
    print(Counter(event_duration_count))

    labels, values = zip(*sorted(Counter(event_duration_count).items()))
    indexes = np.arange(len(labels))
    width = 1

    plt.bar(indexes, values, width)
    plt.xticks(indexes + width * 0.5, labels)
    plt.show()







#sort_dataset(dataset='/home/kamivao/Downloads/lentanews/lentanews_split/model4/hazards_dataset.tsv')
#resort_long_timelines(input_file='/home/kamivao/Downloads/lentanews/lentanews_split/model4/run5/advanced_merge_result.tsv')
extract_context(hazards_dataset='/home/kamivao/Downloads/lentanews/lentanews_split/model4/hazards_dataset.tsv',
                automerged_timelines='/home/kamivao/Downloads/lentanews/lentanews_split/model4/run5/advanced_merge_result.tsv')