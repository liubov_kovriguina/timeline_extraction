import codecs, os, sys
import csv
from urllib.parse import urlparse
import collections
from collections import Counter, defaultdict, namedtuple
import string
from nltk import word_tokenize
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from gensim.models import KeyedVectors
import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from matplotlib import pyplot as plt
import seaborn as sns
sns.set()
import time
from urllib.parse import urlparse
import re
import datetime
import gensim
from scipy.spatial import distance
#https://radimrehurek.com/gensim/models/keyedvectors.html#gensim.models.keyedvectors.WordEmbeddingsKeyedVectors.similarity n_similarity()

def parseObjectLabel(uri):
    objectLabel = urlparse(uri)
    dateSegment = objectLabel.path
    return dateSegment


def getDate(dateSegment):
    class_name = 'DateTuple'
    fields = 'year month day'
    DateTuple = namedtuple(class_name, fields)
    pathItems = re.split(r'/', dateSegment)
    date_i = datetime.date(int(pathItems[2]), int(pathItems[3]), int(pathItems[4]))
    return date_i

def subtract(excessive, total):
    efficient = []
    for ex in excessive:
        if ex in total:
            total.remove(ex)
    efficient = total
    return efficient


def main(datasource, datasource_titles, pairwise_sim_outfile, timelines_outfile, all_timelines_outfile, excessive_timelines_indices_outfile):
    try:
        # reading file with feature vectors and doc labels: 1 100 dim doc2vec vector per line,  last item  is uri (label)
        file = codecs.open(datasource, mode='r', encoding='utf-8')
        data = file.readlines()
        vectors = []
        labels = []

        # reading data into internal representation
        for line in data:
            line = line.split(',')
            vector = np.asarray(line[0:-1], dtype='float64')
            label = line[-1]
            vectors.append(vector)
            labels.append(label)

        title_vectors_dict = {}
        titles_vectors = codecs.open(datasource_titles, mode='r', encoding='utf-8')
        titles_vectors_lines = titles_vectors.readlines()
        tt_vectors = []
        for line in titles_vectors_lines:
            line = line.split(',')
            title_vectors_dict.update({str(line[-1]): line[0:-2]})

        print("Vectors loaded")
        print("Datasource reading finished")
        # calculating pairwise document similarity and writing it to outfile
        # timeline extraction
        similarities_array = []
        f = codecs.open(pairwise_sim_outfile, mode='w+', encoding='utf-8')
        tsv_writer = csv.writer(f, delimiter='\t')

        timelines = []      ### a list for storing all extracted timelines
        timelines_length_distribution = []
        invalid_vectors = []

        start_time = time.time()
        for i in range(len(vectors)):
            timeline = []
            for j in range(i + 1, len(vectors)):
                vi = vectors[i].reshape(1, -1)
                vj = vectors[j].reshape(1, -1)
                similarity = cosine_similarity(vi, vj)


                # pairwise similarity value is written to outfile
                sim_info = [str(similarity), labels[i], labels[j]]
                tsv_writer.writerow(sim_info)

                float_sim = similarity[0]
                if float_sim[0] > 0.895:
                    #print("ok!")
                    # additional check is done for titles' similarity, should exceed 0.7

                    vectori = np.asarray(title_vectors_dict.get(labels[i]), dtype='float64')
                    vectorj = np.asarray(title_vectors_dict.get(labels[j]), dtype='float64')
                    vi_titles = vectori.reshape(1, -1)
                    vj_titles = vectorj.reshape(1, -1)

                    #titles_similarity = cosine_similarity(vi_titles, vj_titles)

                    titles_similarity = 1 - distance.cosine(vi_titles, vj_titles)

                    if np.isnan(titles_similarity):
                        titles_similarity = 0
                        invalid_vectors.append(labels[i])
                        invalid_vectors.append(labels[j])
                    #print(titles_similarity)
                    if titles_similarity > 0.2:

                        # timeline extraction starts here with parsing uri labels for documents with cosine similarity value exceeding 0.45
                        datesList = []
                        labelsList = [labels[i], labels[j]]
                        for item in labelsList:
                            dateSegment = parseObjectLabel(item)
                            date_i = getDate(dateSegment)
                            datesList.append(date_i)

                        # if the delta between publishing dates is less, than 7 days, uri's are added to timeline

                        delta = datesList[0] - datesList[1]
                        diff = abs(delta.days)
                        if diff < 10:
                            #print(titles_similarity)
                            #print(labels[i], labels[j])
                            timeline.append(labels[i].rstrip())
                            timeline.append(labels[j].rstrip())
                            print("uri 1, uri 2, text similarity, title similarity", labels[i], labels[j], similarity, titles_similarity)
            # if timeline for doc_id == 1 is not empty, it is added to timelines list
            if len(timeline) != 0:
                timelines.append(set(timeline))
                print("Timeline %d appended" % len(timelines))
                timelines_length_distribution.append(len(timeline))
        print("Number of extracted timelines: ", len(timelines))
        print("Timelines extraction finished in %s seconds " % (time.time() - start_time))
        with codecs.open(all_timelines_outfile, mode='w+', encoding='utf-8') as alltl_writer:
            for tl in timelines:
                alltl_writer.write(str(tl) + '\n')

        # filtering of excessive timelines from "timelines" starts here
        print("Filtering of excessive timelines has started")
        excessive_timelines_indices = []
        for i in range(len(timelines)):
            if len(timelines[i]) != 0:
                # delete shorter timelines
                for j in range(i + 1, len(timelines)):
                    if (timelines[i].issuperset(timelines[j]) and not (j in excessive_timelines_indices)):
                        excessive_timelines_indices.append(j)
                    elif (timelines[i].issubset(timelines[j]) and not (i in excessive_timelines_indices)):
                        excessive_timelines_indices.append(i)

        #excessive_timelines_indices_set = set(excessive_timelines_indices)
        with codecs.open(excessive_timelines_indices_outfile, mode='w+', encoding='utf-8') as extl_writer:
            for extl in excessive_timelines_indices:
                extl_writer.write(str(extl) + '\n')
        print("Excessive timelines indices: ", excessive_timelines_indices)

        excessive_timelines = []
        for index in excessive_timelines_indices:   ##excessive_timelines_indices_set:
            excessive_timelines.append(timelines[index])

        #print(excessive_timelines)

        efficient = subtract(excessive=excessive_timelines, total=timelines)
        # https://www.tutorialspoint.com/How-to-sort-a-Python-date-string-list
        for tl in efficient:
            sorted_timeline = sorted(tl, reverse=False)
        print("Efficient timelines: ", efficient)
        with codecs.open(timelines_outfile, mode='w+', encoding='utf-8') as tl_writer:
            for eftl in efficient:
                tl_writer.write(str(eftl) + '\n')

        tl_writer.close()
        extl_writer.close()
        alltl_writer.close()
        f.close()
        print(Counter(timelines_length_distribution))
        print("Number of efficient timelines: ", len(efficient))
        print(invalid_vectors)

    except OSError as e:
        print(e)
        raise


if __name__ == "__main__":
    main(datasource='/home/kamivao/Downloads/lentanews/lentanews_split/model4/model4_text_vectors.tsv',
         datasource_titles='/home/kamivao/Downloads/lentanews/lentanews_split/model4/model4_title_vectors.tsv',
         pairwise_sim_outfile='/home/kamivao/Downloads/lentanews/lentanews_split/model4/run5/similarities_matrix.tsv',
         timelines_outfile='/home/kamivao/Downloads/lentanews/lentanews_split/model4/run5/efficient_timelines_uri.tsv',
         all_timelines_outfile='/home/kamivao/Downloads/lentanews/lentanews_split/model4/run5/all_timelines_uri.tsv',
         excessive_timelines_indices_outfile='/home/kamivao/Downloads/lentanews/lentanews_split/model4/run5/excessive_timelines_indices.tsv')





#/home/kamivao/Downloads/lentanews/lentanews_split/hazards18354_docvec_wf_vec100_win10_count5.csv'
