from news_clustering import url_check, decode
import codecs, os
import csv
from urllib.parse import urlparse
import collections
import sys
import time
import string
from nltk import word_tokenize
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from gensim.models import FastText  # FIXME: why does Sphinx dislike this import?
from gensim.test.utils import common_texts  # some example sentences
from gensim.test.utils import get_tmpfile
from gensim.models.wrappers.fasttext import FastText as FT_wrapper
from gensim.models import KeyedVectors
from gensim.test.utils import datapath
import gensim.models
import numpy as np
import re, string

def preprocessing(filename):
    datafile = codecs.open(filename, mode='r+', encoding='utf-8')
    output_filepath = '/home/kamivao/Downloads/lentanews/lentanews_split/model4/hazards_dataset.tsv'
    outfile = codecs.open(output_filepath, mode='w+',  encoding='utf-8')
    tsv_oufile = csv.writer(outfile, delimiter='\t')
    translator = str.maketrans('', '', string.punctuation)
    uri_list, title_collection, wordforms_doccollection = [], [], []
    for line in datafile:
        line = line.replace('\"\"', '')
        linelist = line.split('\t')
        if len(linelist) == 3:

            title = linelist[1]
            text = linelist[2]
            if len(title) == 0:
                get_title = re.split(r'([^\s-]\"[А-Я])', text)
                if len(get_title) > 1:
                    #print(get_title[1])
                    s = text.find(get_title[1])
                    #print(s)
                    title_end_index = text.index(get_title[0])
                    text_start_index = text.index(get_title[1]) + 2
                    #print(text[0:s + 1])
                    #print(text[text_start_index:])
                    text_ex = text[text_start_index:]
                    title_ex = text[0:line.index(get_title[1]) + 1]
                    text_lcased = text_ex.lower()
                    title_lcased = title_ex.lower()
                    cleaned_text = text_lcased.translate(translator)
                    cleaned_title = title_lcased.translate(translator)
                    #tokens_text = [x.strip() for x in cleaned_text.split()]
                    #tokens_title = [x.strip() for x in cleaned_title.split()]
                    #wordforms_doccollection.append(tokens_text)
                    #title_collection.append(tokens_title)
                    #uri_list.append(linelist[0])
            else:
                text_lcased = text.lower()
                title_lcased = title.lower()
                cleaned_text = text_lcased.translate(translator)
                cleaned_title = title_lcased.translate(translator)
                #tokens_text = [x.strip() for x in cleaned_text.split()]
                #tokens_title = [x.strip() for x in cleaned_title.split()]
                #wordforms_doccollection.append(tokens_text)
                #title_collection.append(tokens_title)
            uri_list.append(linelist[0])
            outfile.write(str(linelist[0])+'\t'+str(cleaned_title)+'\t'+str(cleaned_text))
    #print(len(wordforms_doccollection), len(title_collection))
    outfile.close()

def validate_content(filename):
    datafile = codecs.open(filename, mode='r+', encoding='utf-8')
    data = datafile.readlines()
    for line in data:
        line_items = line.split('\t')
        print(line_items[1])
        #if len(line_items) != 3:
         #   print(line_items)
    print(len(data))


def train_fasttext_gensim_model(training_file):
    datafile = codecs.open(training_file, mode='r+', encoding='utf-8')
    data = datafile.readlines()
    uri_indices = []
    title_collection = []
    text_collection = []
    for line in data:
        linelist = line.split('\t')
        uri_indices.append(linelist[0])
        title_tokens = linelist[1].split(' ')
        title_collection.append(title_tokens)
        text_tokens = linelist[2].split(' ')
        text_collection.append(text_tokens)

    training_collection = text_collection + title_collection

    model_hazards = FastText(size=100, window=5, min_count=1)  # instantiate
    model_hazards.build_vocab(sentences=training_collection)
    model_hazards.train(sentences=training_collection, total_examples=len(training_collection), epochs=50)  # train

    model_hazards.save('/home/kamivao/Downloads/lentanews/lentanews_split/model4/100-5-1-50-model_gensim_ft_hazards')


def load_model(datapath='/home/kamivao/Downloads/lentanews/lentanews_split/model4/hazards_dataset.tsv'):

    datafile = codecs.open(datapath, mode='r',  encoding='utf-8')
    data = datafile.readlines()

    loaded_model = FastText.load(
        '/home/kamivao/Downloads/lentanews/lentanews_split/model4/100-5-1-50-model_gensim_ft_hazards')

    text_doc_embeddings_dict = {}
    titles_doc_embeddings_dict = {}

    tokens_vectors_single_document = []
    tokens_vectors_single_title = []

    for line in data:
        #print(line)
        linelist = line.split('\t')
        tokens = linelist[2].split(' ')
        title_tokens = linelist[1].split(' ')

        for token in tokens:
            vector = loaded_model.wv[token]
            tokens_vectors_single_document.append(vector)
            average_doc_vector = np.mean(tokens_vectors_single_document, axis=0)
            text_doc_embeddings_dict.update({linelist[0]: average_doc_vector})
            tokens_vectors_single_document = []

        for tt in title_tokens:
            vector_tt = loaded_model.wv[tt]
            tokens_vectors_single_title.append(vector_tt)
            average_title_vector = np.mean(tokens_vectors_single_title, axis=0)
            titles_doc_embeddings_dict.update({linelist[0]: average_title_vector})
            tokens_vectors_single_title = []

    #writing vectors to file

    f1 = codecs.open('/home/kamivao/Downloads/lentanews/lentanews_split/model4/model4_text_vectors.tsv', mode='w+', encoding='utf-8')
    tsv_writer1 = csv.writer(f1, delimiter=',')
    for key, val in text_doc_embeddings_dict.items():
        inferred_vector = np.array(val.tolist())
        data_line = np.append(inferred_vector, str(key))

        tsv_writer1.writerow(data_line)
    f1.close()

    f2 = codecs.open('/home/kamivao/Downloads/lentanews/lentanews_split/model4/model4_title_vectors.tsv', mode='w+', encoding='utf-8')
    tsv_writer2 = csv.writer(f2, delimiter=',')
    for key, val in titles_doc_embeddings_dict.items():
        inf_vector = np.array(val.tolist())
        d_line = np.append(inf_vector, str(key))
        tsv_writer2.writerow(d_line)
    f2.close()

    print("!!!")

   # print(loaded_model)
    #print(loaded_model.wv.most_similar('землетрясение'))
    #print(loaded_model.wv.most_similar('наводнение'))
    #vector = loaded_model.wv['землетрясение']
    #print(vector)


load_model(datapath='/home/kamivao/Downloads/lentanews/lentanews_split/model4/hazards_dataset.tsv')



#preprocessing(filename='/home/kamivao/Downloads/lentanews/lentanews_split/hazards_dataset_3.tsv')

#validate_content(filename='/home/kamivao/Downloads/lentanews/lentanews_split/model4/hazards_dataset.tsv')

#train_fasttext_gensim_model(training_file='/home/kamivao/Downloads/lentanews/lentanews_split/model4/hazards_dataset.tsv')
