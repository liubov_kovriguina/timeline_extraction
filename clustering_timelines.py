import codecs, os
import csv
from urllib.parse import urlparse
import collections
import sys
import time
import string
from nltk import word_tokenize
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
import numpy as np



def csv_extract(datasource):

    csvfile = codecs.open(datasource, mode='r', encoding='utf-8', errors='ignore')
    uri_list, title_list, wordforms_doccollection = [], [], []
    translator = str.maketrans('', '', string.punctuation)


    for line in csvfile:
        linelist = line.split('\t')
        uri_list.append(linelist[0])
        title_list.append(linelist[1].lower())
        content = linelist[2].translate(translator)
        content = content.lower()
        wordforms_doccollection.append(content)

    print(len(uri_list), len(wordforms_doccollection), len(title_list))
    csvfile.close()

    return uri_list, title_list, wordforms_doccollection


def doc2vec_modeling(uri_list, wordforms_doccollection, doc2vec_wordforms_outfile):
    document_ids_list = uri_list
    translator = str.maketrans('', '', string.punctuation)
    wf_document_stream = [word_tokenize(doc) for doc in wordforms_doccollection]

    wf_documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(wordforms_doccollection)]
    print(wf_documents[2])



    wf_model = Doc2Vec(wf_documents, vector_size=100, window=10, min_count=5, workers=4, epochs=40)

    #write document embeddings for lemmas with uri as label to outfile


    tsv_output_wordforms = csv.writer(doc2vec_wordforms_outfile, delimiter=',')
    #ranks = []
    #second_ranks = []
    #array_of_sims = []
    for doc_id in range(len(wf_documents)):
        index = doc_id
        inferred_vector = wf_model.infer_vector(wf_documents[doc_id].words)
        inferred_vector = np.array(inferred_vector.tolist())
        data_line = np.append(inferred_vector, str(uri_list[index]))
        tsv_output_wordforms.writerow(data_line)
        #sims = wf_model.docvecs.most_similar([inferred_vector], topn=len(wf_model.docvecs))
        #rank = [docid for docid, sim in sims].index(doc_id)
        #ranks.append(rank)
        #array_of_sims.append(sims)

        #second_ranks.append(sims[1])
        #sims = []

    #print(collections.Counter(ranks))


def main():

    try:
        outpath = '/home/kamivao/Downloads/lentanews'

        if not os.path.exists(outpath):
            os.makedirs(outpath)

        with open(os.path.join(outpath, 'stats.txt'), mode='a+', encoding='utf-8') as stats:
            start_time = time.time()
            uri_list, title_list, wordforms_doccollection = csv_extract(datasource='/home/kamivao/Downloads/lentanews/lentanews_split/hazards_dataset_3.tsv',)
            stats.write("data loading finished in %s seconds " % (time.time() - start_time) + "\n")
            doc2vec_modeling(uri_list, wordforms_doccollection,
                             doc2vec_wordforms_outfile=open('/home/kamivao/Downloads/lentanews/lentanews_split/hazards18354_docvec_wf_vec100_win10_count5.csv',
                                                            mode='w+', encoding='utf-8'))

            stats.write("doc2vec modeling finished in %s seconds " % (time.time() - start_time) + "\n")
            start_time = time.time()
            stats.close()

    except OSError as e:
        print(e)
        raise


if __name__ == "__main__":
    main()