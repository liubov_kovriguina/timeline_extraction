import time


def countdown(num):
    print
    print('Starting')
    while num > 0:
        yield num
        num -= 1

val = countdown(5)
next(val)

#https://realpython.com/introduction-to-python-generators/